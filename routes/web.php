<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', array('as'=> 'front.home', 'uses' => 'FrontController@index'));

// contact route
Route::post('/contact', array('as'=> 'front.contact', 'uses' => 'FrontController@contact'));
Route::get('product/{slug}', array('as'=> 'front.product', 'uses' => 'FrontController@productIndex'));

Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');

Route::group(['prefix' => 'admin'], function () {

	Route::resource('sliders', 'Admin\SliderController');
	Route::resource('products', 'Admin\ProductController');
	Route::resource('contacts', 'Admin\ContactController');
	Route::resource('settings', 'Admin\SettingController');

	Route::post('productImage/store', array('as'=> 'productsImages.store', 'uses' => 'Admin\ProductImgController@store'));
	Route::get('productImage/create/{id}', array('as'=> 'productsImages.create', 'uses' => 'Admin\ProductImgController@create'));
	Route::get('productImage/{id}', array('as'=> 'productsImages.index', 'uses' => 'Admin\ProductImgController@index'));
	Route::delete('productImage/delete/{id}', array('as'=> 'productsImages.destroy', 'uses' => 'Admin\ProductImgController@destroy'));

});