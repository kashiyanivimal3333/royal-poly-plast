<?php

use Illuminate\Database\Seeder;
use App\Setting;
use App\User;

class AdminSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
	        	[
	        		'name' => 'Admin Title',
                    'slug' => 'admin-title',
                    'value' => 'Royal Poly Plast'
	        	],
	        	[
	        		'name' => 'Admin Logo',
                    'slug' => 'admin-logo',
	        		'value' => 'logo.png'
	        	],
                [
                    'name' => 'Meta Title',
                    'slug' => 'meta-title',
                    'value' => 'test'
                ],
                [
                    'name' => 'Meta Description',
                    'slug' => 'meta-description',
                    'value' => 'test'
                ],
                [
                    'name' => 'Fevicon',
                    'slug' => 'fevicon',
                    'value' => 'test'
                ],
                [
                    'name' => 'Address',
                    'slug' => 'address',
                    'value' => 'test'
                ],
                [
                    'name' => 'Mobile_No',
                    'slug' => 'mobile-no',
                    'value' => 'test'
                ],
                [
                    'name' => 'Email_Id',
                    'slug' => 'email-id',
                    'value' => 'test'
                ],
                [
                    'name' => 'About_you',
                    'slug' => 'about-you',
                    'value' => 'test'
                ],
                [
                    'name' => 'Footer',
                    'slug' => 'footer',
                    'value' => 'test'
                ],
                [
                    'name' => 'Contact Us',
                    'slug' => 'contact-us',
                    'value' => 'test'
                ],
	        	
        	];

        foreach ($settings as $key => $value) {
        	Setting::create($value);
        }

        User::where('email','admin@gmail.com')->delete();
        User::create(
            [
                'name' => 'Eugene Leoncio', 
                'email' => 'admin@gmail.com', 
                'password' => bcrypt('123456'), 
            ]);
    }
}
