<ul class="slides">
	@if(!empty($slider) && $slider->count())
		@foreach($slider as $key => $slider)
			<li>
				<img src="/upload/slider/{{ $slider->image }}"/>
			</li>
		@endforeach
	@endif
</ul>