<div class="container">
	<div class="row">

		<div class="col-md-4 col-md-4 col-sm-12">
			<div class="footer-heading">
				<h3><span>about</span> us</h3>
				<p>Founded in 2010, by Mr. Nikunj Dadhaniya, M-TORR irrigation since then has proved to be the premiere distributor and warehouse point for PP Sanitary Products & DRIP Products throughout the nation.</p>
				<p>To explore strange new worlds to seek out new life and new civilizations to boldly go where no man has gone before. It's time to play the music.</p>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
			</div>
		</div>

		<div class="col-md-4 col-md-4 col-sm-12">
			<div class="footer-heading">
				<h3><span>Image</span> Gallery</h3>
				<div class="insta">
					<ul>
						@foreach($productRandom as $productRand)
							<img src="/upload/product/{{ $productRand->image }}" alt="">
						@endforeach
					</ul>
				</div>
			</div>
		</div>

		<div class="col-md-4 col-md-4 col-sm-12">
			<div class="row">
				<div class="col-lg-12">
					<div class="footer-heading">
						<h3><span>Address</span></h3>
						<p><b>Location :</b> {!! $settingsData['address'] !!}</p>
						<p><b>Mobile No :</b> {!! $settingsData['mobile-no'] !!}</p><br>
						<p><b>Email :</b> {!! $settingsData['email-id'] !!}</p>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="icon-bar">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
