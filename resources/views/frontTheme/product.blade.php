
<div class="container">
	<div class="row">

		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<div class="portfolio-heading">
				<h2>product</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent metus magna,malesuada porta elementum vitae.</p>
			</div>
		</div>

	</div>
</div>

<div class="portfolio-thumbnail">
	<div class="container">
		<div class="row">
			@foreach($product as $key => $pro)
				<article class="col-xs-12 col-sm-6 col-md-4">
		            <div class="panel panel-default">
		                <div class="panel-body">
		                    <a href="{{ route('front.product',$pro->slug) }}" class="zoom" title="{{ $pro->name }}">
		                        <img src="/upload/product/{{ $pro->image }}"/>
		                        <span class="overlay"><i class="fa fa-arrows-alt" aria-hidden="true"></i></span>
		                    </a>
		                </div>
		                <div class="panel-footer">
		                    <h4><a href="#">{{ $pro->name }}</a></h4>
		                </div>
		            </div>
		        </article>
	        @endforeach
		</div>
	</div>
</div>

<div id="about-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-10 text-center col-md-offset-1">
				<div class="about-bg-heading">
					<h1>action is the foundational key to all success</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="cover"></div>
</div>

<!--contact form-->
<div id="get-touch">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-12">
				<div class="get-touch-heading">
					<h2>contact us</h2>
					<br />
					{!! nl2br($settingsData['contact-us']) !!}
					<br /><br />
				</div>
			</div>
		</div>

		<div class="content">
				<div class="form">
				<div id="sendmessage">Your message has been sent. Thank you!</div>
				<div id="errormessage"></div>
				{!! Form::open(array('route'=>'front.contact','method'=>'POST','class'=>'form contactForm')) !!}

					<div class="col-md-4">
						<div class="form-group">
	                        {!! Form::text('name',null, ['class' => 'form-control','placeholder' => 'Your Name','data-rule' => 'required','data-msg' => 'Please enter name','rows' => '3']) !!}
							<div class="validation"></div>
	                    </div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
	                        {!! Form::email('email_id',null, ['class' => 'form-control','placeholder' => 'Your Email','data-rule' => 'required','data-msg' => 'Please enter email']) !!}
							<div class="validation"></div>
	                    </div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
	                        {!! Form::text('mobile_no',null, ['class' => 'form-control','placeholder' => 'Your Contact No','data-rule' => 'required', 'data-msg' => 'Please enter contact no']) !!}
							<div class="validation"></div>
	                    </div>
					</div>

					<div class="col-md-12">
						<div class="form-group">
	                        {!! Form::textarea('message',null, ['class' => 'form-control','placeholder' => 'Your Message','data-rule' => 'required','data-msg' => 'Please enter message','rows' => '8']) !!}
							<div class="validation"></div>
	                    </div>
					</div>

	                <div class="submit text-center">
						<button type="submit" class="btn btn-default">Send Now</button>
					</div>

                {!! Form::close() !!}
			</div>
		</div>
	</div>
</div>