@extends('adminTheme.default')

@section('title')
Manage Slider
@endsection

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-sliders" aria-hidden="true"></i> Manage Slider
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Slider</li>
  </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h1 class="page-header">
            </h1>
        </div>
        <div class="pull-right">
        	<a href="{{ route('sliders.create') }}"><button class="btn btn-success btn-flat" data-toggle="tooltip" title="Create New Slider !"><i aria-hidden="true" class="fa fa-plus"></i></button></a>
        </div>
    </div>
</div><br>
<div class="box box-info">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <h3 class="box-title">Slider List</h3>
    </div>
    <div class="box-body">
    <table class="table table-bordered">
    <thead>
        <tr>
            <th width="50px">No</th>
            <th>image</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
    @if(!empty($slider) && $slider->count())
        @foreach($slider as $key => $value)
        <tr>
            <td>{{ ++$key }}</td>
            <td><img src="/upload/slider/{{ $value->image }}" style="width:100px;height:100px;"></td>
            <td>
                @include('Admin.Slider.view')
                <a href="{{ route('sliders.show',$value->id) }}" class="btn btn-sm btn-info btn-flat" data-toggle="modal" data-target="#myModal-{{ $value->id }}"><i aria-hidden="true" class="fa fa-eye" data-toggle="tooltip" title="View !"></i></a>
                <button class="btn btn-danger btn-sm btn-flat remove-crud" data-id="{{ $value->id }}" data-toggle="tooltip" data-placement="top" title="Delete" data-action="{{ route('sliders.destroy',$value->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </td>
        </tr>
        @endforeach
    @endif
</div>
    </tbody>
</table>
<div class="text-center">
    @if(!empty($slider) && $slider->count())
    {!! $slider->appends(Input::all())->render() !!}
    @endif
</div>
</div>
</section>
@endsection