<div class="modal fade" id="myModal-{{ $value->id }}" role="dialog">
    <div class="modal-dialog" style="width:80% !important;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Slider image</h4>
        </div>
        <div class="modal-body">
            <div class="col-lg-12 text-center">
                <img src="/upload/slider/{{ $value->image }}" style="width:100%;">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>