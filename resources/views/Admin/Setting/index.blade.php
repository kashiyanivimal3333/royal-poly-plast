@extends('adminTheme.default')

@section('title')
    Setting
@endsection

@section('content')
<section class="content">
<div class="box box-info">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="ion ion-clipboard"></i>
        <h3 class="box-title">Setting</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="modal-body">
                    {!! Form::open(array('route'=>'settings.store','method'=>'POST','files'=>'true','enctype'=>'multipart/form-data')) !!}
                    <div class="row">
                        <div class="col-lg-9">
                           <div class="form-group">
                            {!! Form::hidden('image_old', $settings['admin-logo']['value']) !!}
                                <label>Title:</label>
                                {!! Form::text($settings['admin-title']['slug'],$settings['admin-title']['value'], ['class'=>'form-control']) !!}
                                @if($errors->has('admin-title'))
                                <div><span style="color:red;">{{ $errors->first('admin-title')}}</span></div>
                                @endif
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label>Logo:</label><br>
                                <img src="/upload/setting/{{ $settings['admin-logo']['value'] }}" style="height: 75px;width: 75px;" /><br><br>
                                {!! Form::file($settings['admin-logo']['slug'],['class'=>'form-control']) !!}
                                @if($errors->has('admin-logo'))
                                <div><span style="color:red;">{{ $errors->first('admin-logo')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label>Fevicon:</label><br>
                                <img src="/upload/setting/{{ $settings['fevicon']['value'] }}" style="height: 75px;width: 75px;" /><br><br>
                                {!! Form::file($settings['fevicon']['slug'],['class'=>'form-control']) !!}
                                 @if($errors->has('fevicon'))
                                <div><span style="color:red;">{{ $errors->first('fevicon')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                           <div class="form-group">
                                <label>Meta Title:</label>
                                {!! Form::text($settings['meta-title']['slug'],$settings['meta-title']['value'], ['class'=>'form-control']) !!}
                                @if($errors->has('meta-title'))
                                <div><span style="color:red;">{{ $errors->first('meta-title')}}</span></div>
                                @endif
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                             <div class="form-group">
                                <label>Meta Descrption:</label>
                                {!! Form::textarea($settings['meta-description']['slug'],$settings['meta-description']['value'], ['class'=>'form-control','rows' => '5']) !!}
                                @if($errors->has('meta-description'))
                                <div><span style="color:red;">{{ $errors->first('meta-description')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                             <div class="form-group">
                                <label>Address:</label>
                                {!! Form::textarea($settings['address']['slug'],$settings['address']['value'], ['class'=>'form-control','rows' => '5']) !!}
                                @if($errors->has('address'))
                                <div><span style="color:red;">{{ $errors->first('address')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                           <div class="form-group">
                                <label>Mobile No. :</label>
                                {!! Form::text($settings['mobile-no']['slug'],$settings['mobile-no']['value'], ['class'=>'form-control']) !!}
                                @if($errors->has('mobile-no'))
                                <div><span style="color:red;">{{ $errors->first('mobile-no')}}</span></div>
                                @endif
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                           <div class="form-group">
                                <label>Email:</label>
                                {!! Form::text($settings['email-id']['slug'],$settings['email-id']['value'], ['class'=>'form-control']) !!}
                                @if($errors->has('email-id'))
                                <div><span style="color:red;">{{ $errors->first('email-id')}}</span></div>
                                @endif
                            </div> 
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-lg-9">
                             <div class="form-group">
                                <label>About You:</label>
                                {!! Form::textarea($settings['about-you']['slug'],$settings['about-you']['value'], ['class'=>'form-control','rows' => '5']) !!}
                                @if($errors->has('about-you'))
                                <div><span style="color:red;">{{ $errors->first('about-you')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                             <div class="form-group">
                                <label>Contact Us:</label>
                                {!! Form::textarea($settings['contact-us']['slug'],$settings['contact-us']['value'], ['class'=>'form-control','rows' => '5']) !!}
                                @if($errors->has('contact-us'))
                                <div><span style="color:red;">{{ $errors->first('contact-us')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-9">
                           <div class="form-group">
                                <label>Footer:</label>
                                {!! Form::text($settings['footer']['slug'],$settings['footer']['value'], ['class'=>'form-control']) !!}
                                 @if($errors->has('footer'))
                                <div><span style="color:red;">{{ $errors->first('admin-title')}}</span></div>
                                @endif
                            </div> 
                        </div>
                    </div>
                    <div class="box-footer clearfix no-border">
                        <button type="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection