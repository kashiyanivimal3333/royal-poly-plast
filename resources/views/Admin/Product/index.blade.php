@extends('adminTheme.default')

@section('title')
Manage Product
@endsection

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-briefcase" aria-hidden="true"></i>
 Manage Product
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Product</li>
  </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h1 class="page-header">
            </h1>
        </div>
        <div class="pull-right">
        	<a href="{{ route('products.create') }}"><button class="btn btn-success btn-flat" data-toggle="tooltip" title="Create New Product !"><i aria-hidden="true" class="fa fa-plus"></i></button></a>

            <button class="btn btn-primary search-modules test btn-flat" data-toggle="tooltip" title="Search !"><i class="fa fa-search"></i>&nbsp;</button>
        </div>
    </div>
</div><br>
@include('Admin.Product.search')
<div class="box box-info">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <h3 class="box-title">Product list</h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
    <thead>
        <tr>
            <th width="50px">No</th>
            <th>Image</th>
            <th>Name</th>
            <th>Description</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
    @if(!empty($product) && $product->count())
        @foreach($product as $key => $value)
        <tr>
            <td>{{ ++$key }}</td>
            <td><img src="/upload/product/{{ $value->image }}" style="width:100px;height:50px;"></td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->description }}</td>
            <td>
                <a href="{{ route('products.edit',$value->id) }}" class="btn btn-info btn-sm btn-flat" data-toggle="tooltip" title="Edit !"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                <a href="{{ route('products.show',$value->id) }}" class="btn btn-success btn-sm btn-flat" data-toggle="tooltip" title="view !"><i class="fa fa-eye" aria-hidden="true"></i></a>

                <button class="btn btn-danger btn-sm btn-flat remove-crud" data-id="{{ $value->id }}" data-toggle="tooltip" data-placement="top" title="Delete" data-action="{{ route('products.destroy',$value->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></button><br><br>

                <a href="{{ route('productsImages.index',$value->id) }}" class="btn btn-success btn-sm btn-flat" data-toggle="tooltip" title="Manage images !">Manage Images</a>
            </td>
        </tr>
        @endforeach
    @else
        <td colspan="6" class="text-center">There are no image.</td>
    @endif
    </tbody>
</table>
</div>
</section>
<div class="text-center">
    @if(!empty($product) && $product->count())
    {!! $product->appends(Input::all())->render() !!}
    @endif
</div>
@endsection
@section('script')
<script type="text/javascript">
    jQuery(".search-modules").click(function(e) {
        
        e.preventDefault();
        jQuery(".filter-panel").toggle("slow");

    });
</script>
@endsection
