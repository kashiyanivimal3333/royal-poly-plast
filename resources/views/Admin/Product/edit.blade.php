@extends('adminTheme.default')

@section('title')
Edit Product
@endsection

@section('content')
<section class="content-header">
  <h1>
    Create Product
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('products.index') }}">Product</a></li>
    <li class="active">Edit</li>
  </ol>
</section>
<section class="content">
<a href="{{ route('products.index') }}" class="btn btn-primary btn-flat pull-right" data-toggle="tooltip" title="Back !"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><br><br>
<div class="box box-primary">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="ion ion-clipboard"></i>
        <h3 class="box-title">Product</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="modal-body">
                    {!! Form::model($product, ['method'=>'PUT', 'route'=>['products.update',$product->id],'files'=>'true','enctype'=>'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-lg-6">
                            {!! Form::hidden('image_old', $product->image) !!}
                           <div class="form-group">
                                <label>Name:</label>
                                {!! Form::text('name',null, ['class'=>'form-control']) !!}
                                @if($errors->has('name'))
                                <div><span style="color:red;">{{ $errors->first('name')}}</span></div>
                                @endif
                            </div> 
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Image:</label>
                                {!! Form::file('image',['class'=>'form-control']) !!}
                                <img src="/upload/product/{{ $product->image }}" style="width:50px;height:50px;"></td>
                                @if($errors->has('image'))
                                <div><span style="color:red;">{{ $errors->first('image')}}</span></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Descrption:</label>
                        {!! Form::textarea('description',null, ['class'=>'form-control']) !!}
                        @if($errors->has('description'))
                        <div><span style="color:red;">{{ $errors->first('description')}}</span></div>
                        @endif
                    </div>
                    <div class="box-footer clearfix no-border">
                        <a href="{{ route('products.index') }}" class="btn btn-danger btn-flat pull-right" style="margin-left: 10px;" data-toggle="tooltip" title="Cancel !"><i class="fa fa-times" aria-hidden="true"></i></a>
                        <button type="submit" class="btn btn-success pull-right btn-flat" data-toggle="tooltip" title="Submit !"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection