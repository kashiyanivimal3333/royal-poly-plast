<div class="box box-success filter-panel {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <h3 class="box-title">Searching</h3>
    </div>
    <div class="box-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[name][value]', 'Name',array('class'=>'control-label')) !!}
                    {!! Form::text('filter[name][value]',Input::get("filter.name.value"),array('class'=>'form-control','placeholder'=>'Enter Name')) !!}{!! Form::hidden('filter[name][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[description][value]', 'Description',array('class'=>'control-label')) !!}
                    {!! Form::text('filter[description][value]',Input::get("filter.description.value"),array('class'=>'form-control','placeholder'=>'Enter Description')) !!}{!! Form::hidden('filter[description][type]','7') !!}
                </div>
            </li>
            <li>
                <div class='form-group'>
                    <br/>
                    <button class="btn btn-success btn-flat"><i class="fa fa-share-square-o" aria-hidden="true"></i> Submit</button>
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>