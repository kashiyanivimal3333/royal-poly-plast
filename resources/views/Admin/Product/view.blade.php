@extends('adminTheme.default')

@section('title')
View Product
@endsection

@section('content')
<section class="content-header">
  <h1>
    View Product
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i>Home</a></li>
    <li><a href="{{ route('products.index') }}">Product</a></li>
    <li class="active">view</li>
  </ol>
</section>
<section class="content">
<a href="{{ route('products.index') }}" class="btn btn-primary btn-flat pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a><br><br>
<div class="box box-primary">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="ion ion-clipboard"></i>
        <h3 class="box-title">View</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            {!! Form::hidden('image_old', $product->image) !!}
                            <div class="form-group">
                                <label>Name:</label><br>
                                {{ $product->name }}
                            </div> 
                        </div>
                        
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Description:</label><br>
                                {{ $product->description }}
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Image:</label><br>
                                 <img src="/upload/product/{{ $product->image }}" style="width:150px;height: 150px;">
                            </div>
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection