@extends('adminTheme.default')

@section('title')
Create Product
@endsection

@section('content')
<section class="content-header">
  <h1>
    Create Product
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('products.index') }}">Product</a></li>
    <li class="active">Create New Product</li>
  </ol>
</section>
<section class="content">
<a href="{{ route('products.index') }}" class="btn btn-primary btn-flat pull-right" data-toggle="tooltip" title="Back !"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><br><br>
<div class="box box-primary">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="fa fa-briefcase" aria-hidden="true"></i>
   
        <h3 class="box-title">Product</h3>
    </div>

    @if($errors->count() > 0)
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
                @foreach($errors->all() as $er )
                    <ul>
                        <li>{{ $er }}</li>
                    </ul> 
                @endforeach
        </div>
    @endif

    <div class="box-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="modal-body">
                    {!! Form::open(array('route'=>'products.store','method'=>'POST','files'=>'true','enctype'=>'multipart/form-data')) !!}
                    <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group">
                                <label>Name:</label>
                                {!! Form::text('name',null, ['class'=>'form-control']) !!}
                            </div> 
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Image:</label>
                                {!! Form::file('image',['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Descrption:</label>
                        {!! Form::textarea('description',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="box-footer clearfix no-border">
                        <a href="{{ route('products.index') }}" class="btn btn-danger btn-flat pull-right" style="margin-left: 10px;" data-toggle="tooltip" title="Cancel !"><i class="fa fa-times" aria-hidden="true"></i></a>
                        
                        <button type="submit" class="btn btn-success pull-right btn-flat" data-toggle="tooltip" title="Submit !"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection