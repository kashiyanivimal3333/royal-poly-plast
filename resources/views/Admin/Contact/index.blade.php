@extends('adminTheme.default')

@section('title')
Manage Contact
@endsection

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-phone" aria-hidden="true"></i> Manage Contact
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Contact</li>
  </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h1 class="page-header">
            </h1>
        </div>
        <div class="pull-right">
        	<!-- <a href=""><button class="btn btn-success"><i aria-hidden="true" class="fa fa-plus"></i> Create New Product</button></a> -->

            <button class="btn btn-primary search-modules btn-flat" data-toggle="tooltip" title="Search !"><i class="fa fa-search"></i></button>

        </div>
    </div>
</div><br>
@include('Admin.Contact.search')
<div class="box box-info">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <h3 class="box-title">Contact List</h3>
    </div>
    <div class="box-body">
    <table class="table table-bordered">
    <thead>
        <tr>
            <th width="50px">No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile No.</th>
            <th>Message</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
    @if(!empty($contact) && $contact->count())
        @foreach($contact as $key => $value)
        <tr>
            <td>{{ ++$key }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email_id }}</td>
            <td>{{ $value->mobile_no }}</td>
            <td>{{ $value->message }}</td>
            <td>
                <button class="btn btn-danger btn-sm btn-flat remove-crud" data-id="{{ $value->id }}" data-toggle="tooltip" data-placement="top" title="Delete" data-action="{{ route('contacts.destroy',$value->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </td>
        </tr>
        @endforeach
    @else
        <td colspan="6" class="text-center">There are no contact.</td>
    @endif
    </tbody>
</table>
</div>
<div class="text-center">
    @if(!empty($contact) && $contact->count())
    {!! $contact->appends(Input::all())->render() !!}
    @endif
</div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    jQuery(".search-modules").click(function(e) {
        
        e.preventDefault();
        jQuery(".filter-panel").toggle("slow");

    });
</script>
@endsection