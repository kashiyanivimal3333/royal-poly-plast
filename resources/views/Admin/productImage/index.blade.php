@extends('adminTheme.default')

@section('title')
Manage Product Image
@endsection

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-briefcase" aria-hidden="true"></i>
 Manage Product Image
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Product Image</li>
  </ol>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
            <h1 class="page-header">
            </h1>
        </div>
        <div class="pull-right">
        	<a href="{{ route('productsImages.create',$id) }}"><button class="btn btn-success btn-flat" data-toggle="tooltip" title="Create New Product Image!"><i aria-hidden="true" class="fa fa-plus"></i></button></a>
        </div>
    </div>
</div><br>
<div class="box box-info">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <h3 class="box-title">Product image list</h3>
    </div>
    <div class="box-body">
        <table class="table table-bordered">
    <thead>
        <tr>
            <th width="50px">No</th>
            <th>Product_Name</th>
            <th>Image</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($productImage) && $productImage->count())
        @foreach($productImage as $key => $value)
            <tr>
                <td>{{ ++$key }}</td>
                <td>{{ $value->productName }}</td>
                <td><img src="/upload/productImage/{{ $value->name }}" style="width:100px;height:50px;"></td>
                <td>
                     <button class="btn btn-danger btn-sm btn-flat remove-crud" data-id="{{ $value->id }}" data-toggle="tooltip" data-placement="top" title="Delete" data-action="{{ route('productsImages.destroy',$value->id) }}"><i class="fa fa-trash" aria-hidden="true"></i></button><br><br>
                </td>
            </tr>
        @endforeach
        @endif
    </tbody>
</table>
</div>
</section>
<div class="text-center">
    @if(!empty($productImage) && $productImage->count())
    {!! $productImage->appends(Input::all())->render() !!}
    @endif
</div>
@endsection
