<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Royal Poly Plast</title>
	<meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
	<meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans|Open+Sans|Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('frontTheme/css/flexslider.css') }}">
	<!-- <link rel="stylesheet" href="{{ asset('frontTheme/css/bootstrap.min.css') }}"> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset('frontTheme/css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('frontTheme/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('frontTheme/css/custom.css') }}">
	<!-- =======================================================
    Theme Name: MyBiz
    Theme URL: https://bootstrapmade.com/mybiz-free-business-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
  <style type="text/css">
  	.navbar-collapse.collapse{
  		display: none !important;
  	}
  </style>
</head>

	<body id="top" data-spy="scroll">

		<!--top header-->
		<header id="home">
			@include('frontTheme.header')
		</header> 

		<div class="product-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1>{{ $productData->name }}</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- slider -->
			@include('front.product.slider')
		<div class="container">
			<div class="row-fluid product-desc-main">
				<div class="col-md-10 product-desc">
					<h3>Description</h3>
				</div>
				<div class="col-md-12">
					<ul>
						<li>{{ $productData->description }}</li>
					</ul>
				</div>
			</div>
		</div>
		<!--footer-->
		<div id="footer">
			@include('frontTheme.footer')
		</div>

		<!--bottom footer-->
		<div id="bottom-footer" class="hidden-xs">
			@include('frontTheme.bottomFooter')
		</div>

		<!-- jQuery -->
		<script src="{{ asset('frontTheme/js/jquery.min.js') }}"></script>
		<!-- <script src="{{ asset('frontTheme/js/bootstrap.min.js') }}"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- <script src="{{ asset('frontTheme/js/jquery.flexslider.js') }}"></script> -->
		<script src="{{ asset('frontTheme/js/jquery.inview.js') }}"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
		<script src="{{ asset('frontTheme/js/script.js') }}"></script>
		<script src="{{ asset('frontTheme/js/contactform.js') }}"></script>
	</body>
</html>
