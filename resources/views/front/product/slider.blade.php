<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="item active">
                <img src="/upload/product/{{ $productData->image }}">
            </div>
            @if(!empty($productImage))
                @foreach($productImage as $key => $value)
                    <div class="item">
                        <img src="/upload/productImage/{{ $value->name }}">
                    </div>
                @endforeach
            @endif
        </div>
        
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
        
        <ul class="carousel-indicators carousel-indicators-item">
            <li data-target="#myCarousel" data-slide-to="0" class="active"><img src="/upload/product/{{ $productData->image }}"></li>
            @if(!empty($productImage))
                @foreach($productImage as $key => $value)
                    <li data-target="#myCarousel" data-slide-to="{{ ++$key }}"><img src="/upload/productImage/{{ $value->name }}"></li>
                @endforeach
            @endif
        </ul>
	</div>
</div>
