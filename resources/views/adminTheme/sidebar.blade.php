<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('adminTheme/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Hi, {{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <?php
        $currentPageURL = URL::current();
        $pageArray = explode('/', $currentPageURL);
        $pageActive = isset($pageArray[4]) ? $pageArray[4] : 'home';
        ?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="{{ $pageActive == 'home' ? 'active' : ''  }}">
            <a href="{{ route('home') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
       
        <li class="{{ $pageActive == 'sliders' ? 'active' : ''  }}">
            <a href="{{ route('sliders.index') }}">
                <i class="fa fa-sliders" aria-hidden="true"></i> <span>Slider</span>
            </a>
        </li>

        <li class="{{ $pageActive == 'products' ? 'active' : ''  }}">
            <a href="{{ route('products.index') }}">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
 <span>Product</span>
            </a>
        </li>

         <li class="{{ $pageActive == 'contacts' ? 'active' : ''  }}">
            <a href="{{ route('contacts.index') }}">
                <i class="fa fa-phone" aria-hidden="true"></i> <span>Contact</span>
            </a>
        </li>
        
      </ul>
    </section>