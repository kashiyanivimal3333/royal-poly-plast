<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Setting;
use File;

class SettingController extends AdminController
{
    public function __construct()
	{
		parent::__construct();
		$this->setting = new Setting;
	}

	public function index()
	{
        $settings = $this->setting->getSettings();
		return view('Admin.Setting.index',compact('settings'));
	}

	public function store(Request $request)
    { 
        
    	$this->validate($request,[
            'admin-title' => 'required',
            // 'admin-logo' => 'required|image',
            'meta-title' => 'required',
            'meta-description' => 'required',
            // 'fevicon' => 'required|image',
            'mobile-no' => 'required',
            'email-id' => 'required',
            'about-you' => 'required',
			'footer' => 'required',
        ],
        [
            'admin-title.required' => 'The title field is required.',
            'admin-logo.required' => 'The admin-logo field is required.',
            'meta-title.required' => 'The meta-title field is required.',
            'meta-description.required' => 'The meta-description field is required.',
            'fevicon.required' => 'The fevicon field is required.',
            'email-id.required' => 'The email-id field is required.',
            'about-you.required' => 'The about-you field is required.',
        	'footer.required' => 'The footer field is required.',
        ]
        );

        $input = $request->all();
        if($request->has('admin-logo')){
        	if(!empty($input['image_old'])){
        		if(File::exists(public_path('/upload/setting/'.$input['image_old']))){
         	   		File::delete(public_path('/upload/setting/'.$input['image_old']));
        		}
            }
            $image = $request->file('admin-logo');
        	$input['admin-logo'] = $image->getClientOriginalName();
        	$destinationPath = public_path('/upload/setting');
        	$image->move($destinationPath, $input['admin-logo']);	
        }
        if($request->has('fevicon')){
            if(!empty($input['image_old'])){
                if(File::exists(public_path('/upload/setting/'.$input['image_old']))){
                    File::delete(public_path('/upload/setting/'.$input['image_old']));
                }
            }
            $image = $request->file('fevicon');
            $input['fevicon'] = $image->getClientOriginalName();
            $destinationPath = public_path('/upload/setting');
            $image->move($destinationPath, $input['fevicon']);   
        }
        $this->setting->updateSettings($input);

        return redirect()->route('settings.index')
                ->with('success','Settings Successfully Update.');
    }
}
