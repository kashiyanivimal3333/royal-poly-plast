<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Slider;
use Validator;
use App\ImageUpload;

class SliderController extends AdminController
{	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->slider = new Slider;
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function create()
	{	
		return view('Admin.Slider.create');
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index(Request $request)
	{
		$slider = $this->slider->getData($request->all());
		return view('Admin.Slider.index',compact('slider'));
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function store(Request $request)
	{	
		$this->validate($request,[
            'image' => 'required|image',
        ]);

		$input = $request->all();

        if($request->hasfile('image')){
        	$input['image'] = ImageUpload::upload('/upload/slider',$request->file('image'));
        }

		 $this->slider->createData($input);
         notificationMsg('success',$this->crudMessage('add','slider'));
         return redirect()->route('sliders.index');
	}
	
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function destroy($id)
    {
        $this->slider->destroyData($id);
        notificationMsg('success',$this->crudMessage('delete','slider'));
        return redirect()->route('sliders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function show($id)
    {   
        $slider = $this->slider->findData($id);
        return view('Admin.Slider.view',compact('slider')); 
    }
}
