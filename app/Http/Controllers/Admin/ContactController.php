<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Contact;

class ContactController extends AdminController
{
	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function __construct()
	{
		parent::__construct();
		$this->contact = new Contact;
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function create()
	{	
		return view('Admin.Contact.create');
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index(Request $request)
	{
		$contact = $this->contact->getData($request->all());
		return view('Admin.Contact.index',compact('contact'));
	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function store(Request $request)
	{	

	}

	/**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function destroy($id)
    {
        $this->contact->destroyData($id);
        notificationMsg('danger',$this->crudMessage('delete','Contact'));
        return redirect()->route('contacts.index');
    }

}