<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Product;
use File;
use App\ImageUpload;

class ProductController extends AdminController
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function __construct()
	{
		parent::__construct();
		$this->product = new Product;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function create()
	{	
		return view('Admin.Product.create');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index(Request $request)
	{

		$product = $this->product->getData($request->all());
	   	return view('Admin.Product.index',compact('product'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function store(Request $request)
	{	
		$this->validate($request,[
			'name' => 'required',
			'description' =>'required',
            'image' => 'required|image',
        ]);

        $input = $request->all();
        $input['slug'] = str_slug($input['name']);

        if($request->hasfile('image')){
            $input['image'] = ImageUpload::upload('/upload/product',$request->file('image'));
        }

        $this->product->createData($input);
        notificationMsg('success',$this->crudMessage('add','product'));
        return redirect()->route('products.index');
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function update($id , Request $request)
    {
        $this->validate($request,[
			'name' => 'required',
			'description' =>'required',
        ]);
		
		$input = $request->all();

        if($request->has('image')){
        	if(!empty($input['image_old'])){
                    ImageUpload::removeFile('/upload/product/'.$input['image_old']);
            }

            $input['image'] = ImageUpload::upload('/upload/product',$request->file('image'));	
        }

		$this->product->updateData($id,$input);
        notificationMsg('success',$this->crudMessage('update','product'));
        return redirect()->route('products.index');   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function edit($id)
    {
        $product = $this->product->findData($id);
        return view('Admin.Product.edit',compact('product'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function show($id)
    {   
        $product = $this->product->findData($id);
        return view('Admin.Product.view',compact('product')); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function destroy($id)
    {   
        $this->product->destroyData($id);
        notificationMsg('success',$this->crudMessage('delete','product'));
        return redirect()->route('products.index');
    }
}