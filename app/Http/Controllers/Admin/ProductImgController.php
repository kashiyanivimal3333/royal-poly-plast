<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\ProductImg;
use App\ImageUpload;
use File;

class ProductImgController extends AdminController
{
	public function __construct()
	{
		parent::__construct();
		$this->productImg = new ProductImg;
	}

	public function index($id, Request $request)
	{
		$productImage = $this->productImg->getData($id);
	   	return view('Admin.productImage.index',compact('productImage','id'));
	}

    public function create($id)
	{	
		return view('Admin.productImage.create', compact('id'));
	}

	public function store(Request $request)
	{	
		$this->validate($request,[
            'name' => 'required|image',
        ]);

        $input = $request->all();

        if($request->hasfile('name')){
            $input['name'] = ImageUpload::upload('/upload/productImage',$request->file('name'));
        }

        $this->productImg->createData($input);
        notificationMsg('success',$this->crudMessage('add','product_image'));
        return redirect()->route('productsImages.index',['id' => $input['product_id']]);
	}

	public function destroy($id)
    {   
        $this->productImg->destroyData($id);
        notificationMsg('success',$this->crudMessage('delete','product_image'));
        return redirect()->back();
    }
}
