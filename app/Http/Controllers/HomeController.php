<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Product;
use App\Contact;
use App\User;

class HomeController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
        $this->user = new User;
        $this->slider = new Slider;
        $this->product = new Product;
        $this->contact = new Contact;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalslider = $this->slider->getTotalSlider();
        $totalproduct = $this->product->getTotalProdut();
        $totalcontact = $this->contact->getTotalContact();
        $totaluser = $this->user->getTotalUser();
        return view('home',compact('totalslider','totalproduct','totalcontact','totaluser'));
    }
}
