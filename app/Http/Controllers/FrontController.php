<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Slider;
use App\Setting;
use App\Product;
use App\ProductImg;

class FrontController extends Controller
{
    public $settingsData;
    
	public function __construct()
    {
        $this->settingsData = Setting::pluck('value', 'slug')->all();
        view()->share('settingsData',$this->settingsData);

        $this->slider = new Slider;
        $this->contact = new Contact;
        $this->product = new Product;
        $this->productImg = new ProductImg;
    }

    public function index()
    {
        $slider = $this->slider->fgetSlider();
        $product = $this->product->fgetProduct();
        $productRandom = $this->product->fgetProductRandom();

    	return view('frontTheme.default',compact('product','productRandom','slider'));
    }

    public function contact(Request $request)
    {
    	$input = $request->all();

    	$this->contact->createData($input);

    	return response()->json(['success' => 'OK']);
    }

    public function productIndex(Request $request,$slug)
    {
        $productData = $this->product->fgetProductWithSlug($slug);

        $productImage = $this->productImg->fgetProductImage($productData->id);

        $productRandom = $this->product->fgetProductRandom();

        return view('front.product.index',compact('productRandom','productData','productImage'));
    }

    
}
