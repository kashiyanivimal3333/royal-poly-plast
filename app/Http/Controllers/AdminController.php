<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function __construct()
	{
		
	} 	 

	public function crudMessage($type, $module)
	{
		switch ($type) {
			case 'add':
				return $module . ' created successfully';
				break;
			case 'delete':
				return $module . ' deleted successfully';
				break;
			case 'update':
				return $module . ' updated successfully';
				break;
			case 'change password':
				return $module . ' password change successfully';
				break;
			
			default:
				# code...
				break;
		}
	}  
}
