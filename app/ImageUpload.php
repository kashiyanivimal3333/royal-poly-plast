<?php

namespace App;

use DB;
use File;
use Image;

class ImageUpload
{
    public static function removeFile($path)
    {
        if(File::exists(public_path($path))){
            File::delete(public_path($path));
        }
    }

    public static function removeDir($path)
    {
        if(File::isDirectory(public_path($path))){
            File::deleteDirectory(public_path($path));
        }
    }

    public static function upload($path, $image)
    {
        $imageName = time().'_'.$image->getClientOriginalName();
        $image->move(public_path($path),$imageName);
        
        return $imageName;
    }
}
