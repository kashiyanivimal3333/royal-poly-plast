<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class ProductImg extends Model
{
	protected $table = 'product_image';

    protected $fillable = ['product_id','name'];

    public function createData($input)
    {
    	return static::create(array_only($input,$this->fillable));
    }

    public function getData($id){

        $data = static::select("product_image.*","products.name as productName")
            ->join("products","products.id","=","product_image.product_id")
            ->where('product_image.product_id',$id)
            ->latest()->paginate(10);

        return $data;
    }

    public function fgetProductImage($product_id)
    {
        return static::select("product_image.*")
                ->where("product_image.product_id","=",$product_id)
                ->orderBy("product_image.id","DESC")
                ->get(); 
    }

    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

}