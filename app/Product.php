<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class Product extends Model
{
   	protected $fillable = ['name','slug','description','image'];

   	public function createData($input)
    {
    	return static::create(array_only($input,$this->fillable));
    }

    public function getData($input)
    {
    	$data = static::select("products.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
        
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];

                    if ($row["type"] == 7) {
                        $data->where("products.".$column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where("products.".$column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->latest()->paginate(10);
    }

    public function findData($id)
    {
    	return static::find($id);
    }

    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function updateData($id,$input)
    {
    	return static::find($id)->update(array_only($input,$this->fillable));
    }

    public function getTotalProdut()
    {
        return static::count();
    }

    public function fgetProduct()
    {
        return static::get();
    }
    
    public function fgetProductRandom()
    {
        return static::orderByRaw("RAND()")->get();  
    }   

    public function fgetProductWithSlug($slug)
    {
        return static::where('slug',$slug)->first();
    }

    
}