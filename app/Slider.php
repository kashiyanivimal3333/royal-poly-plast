<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{

    protected $fillable = ['image'];

    public function createData($input)
    {
    	return static::create(array_only($input,$this->fillable));
    }

    public function getData($input)
    {
    	$data = static::select("sliders.*");
        return $data = $data->latest()->paginate(10);
    }
    
    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function findData($id)
    {
        return static::find($id);
    }

    public function getTotalSlider()
    {
        return static::count();
    }

    public function fgetSlider()
    {
        return static::get();
    }
}