<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class Contact extends Model
{
    protected $fillable = ['name','email_id','mobile_no','message'];

    public function createData($input)
    {
    	return static::create(array_only($input,$this->fillable));
    }

    public function getData($input)
    {
    	$data = static::select("contacts.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
        
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];

                    if ($row["type"] == 7) {
                        $data->where("Contacts.".$column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where("Contacts.".$column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->latest()->paginate(10);
    }
    
    public function destroyData($id)
    {
        return static::find($id)->delete();
    }

    public function getTotalContact()
    {
        return static::count();
    }
}
